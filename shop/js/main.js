$(function () {
  $(".top__slider").slick({
    dots: true,
    arrows: false,
    fade: true,
    // autoplay: true,
    autoplaySpeed: 5000,
  });
});

class BindEvents {
  constructor() {
    this.options = {
      search: "",
      sort: "default",
    };
    this.searchText = document.getElementById("myInput");
    this.sortSelect = document.querySelector(".sortSelect");
    this.bindEventOnInput();
    this.bindEventOnSort();
  }

  set sort(value) {
    this.options.sort = value;
    this.filterAndRender();
  }

  set search(value) {
    this.options.search = value;
    this.filterAndRender();
  }

  filterAndRender() {
    const { search, sort } = this.options;
    const filteredItems = items.filter((item) => {
      return item.name.toLowerCase().includes(search.toLowerCase());
    });

    if (sort === "default") {
      return this.renderCards(filteredItems);
    }

    filteredItems.sort((a, b) => {
      return sort === "asc" ? a.price - b.price : b.price - a.price;
    });
    this.renderCards(filteredItems);
  }


  bindEventOnSort() {
    this.sortSelect.addEventListener("click", (event) => {
      event.stopPropagation();
    });

    this.sortSelect.oninput = (event) => {
      this.sort = event.target.value;
    };

    const searchBtn = document.querySelector(".searchInput-seting__btn");

    searchBtn.onclick = (e) => {
      this.sortSelect.classList.toggle("sortSelect-active");
    };
  }

  bindEventOnInput() {
    this.searchText.oninput = (event) => {
      this.search = event.target.value;
    };
  }
}

class FilterRender {
  constructor() {
    this.options = [
      {
        title: "Price",
        options: this.initialPrice,
        setter: () => {},
      },
      {
        title: "Memory",
        options: this.initialMemory,
        setter: () => {},
      },
      {
        title: "Color",
        options: this.initialColor,
        setter: () => {},
      },
      {
        title: "Os",
        options: this.initialOs,
        setter: () => {},
      },
      {
        title: "Display",
        options: this.initialDisplay,
        setter: () => {},
      },
    ];
    this.renderFilters();
  }

  get initialPrice() {
    const sortedArr = [...items].sort((a, b) => a.price - b.price);
    return [sortedArr[0].price, sortedArr[sortedArr.length - 1].price];
  }

  get initialColor() {
    return items.reduce((acc, item) => {
      item.color.forEach((colorInArr) => {
        if (acc.includes(colorInArr)) return;
        acc.push(colorInArr);
      });
      return acc;
    }, []);
  }

  get initialMemory() {
    return items.reduce((acc, item) => {
      if (!acc.includes(item.storage) && item.storage !== null) {
        acc.push(item.storage);
      }
      return acc;
    }, []);
  }

  get initialOs() {
    return items.reduce((acc, item) => {
      if (!acc.includes(item.os) && item.os !== null) {
        acc.push(item.os);
      }
      return acc;
    }, []);
  }

  get initialDisplay() {
    return items.reduce((acc, item) => {
      if (!acc.includes(item.display) && item.display !== null) {
        acc.push(item.display);
      }
      return acc;
    }, []);
  }

  renderFilters() {
    const filtersContainer = document.querySelector(".filters");
    const searchInputFilterBtn = document.querySelector(
      ".searchInput-filter__btn"
    );
    searchInputFilterBtn.onclick = () => {
      const isActive = filtersContainer.classList.toggle("filters--active");
    };

    this.options.forEach((data) => {
      const block = document.createElement("div");
      block.className = "filters_block";

      const header = document.createElement("div");
      header.className = "filters_header";
      header.innerHTML = `
      <p>${data.title}</p>
      <i class="fas fa-chevron-rigth"></i>`;

      header.onclick = () => {
        const isActive = container.classList.toggle(
          "filters_container--active"
        );

        header.innerHTML = `  
          <p>${data.title}</p>
          <i class="fas fa-chevron-${isActive ? "down" : "right"}"></i>`;
      };

      const container = document.createElement("div");
      container.className = "filters_container";

      if (data.title === "Price") {
        // FROM
        const labelFrom = document.createElement("label");
        labelFrom.innerText = "From";

        const inputFrom = document.createElement("input");
        inputFrom.value = data.options[0];
        inputFrom.oninput = (e) => {
          console.log(e.target.checked);
          console.log(data);
        };
        labelFrom.appendChild(inputFrom);

        // TO
        const labelTo = document.createElement("label");
        labelTo.innerText = "To";

        const inputTo = document.createElement("input");
        inputTo.value = data.options[1];
        inputTo.oninput = (e) => {
          console.log(e.target.checked);
          console.log(data);
        };
        labelTo.appendChild(inputTo);
        container.append(labelFrom, labelTo);
      } else {
        data.options.forEach((option) => {
          if (data.title === "Price") {
          }
          const label = document.createElement("label");
          label.innerText = option;

          const input = document.createElement("input");
          input.type = "checkbox";
          input.onchange = (e) => {
            console.log(e.target.checked);
            console.log(data);
            console.log(option);
          };

          label.appendChild(input);
          container.appendChild(label);
        });
      }

      block.append(header, container);
      filtersContainer.appendChild(block);
    });
  }
}

const filterrender = new FilterRender();

class Card extends BindEvents {
  constructor() {
    super();
    this.container = document.querySelector(".cards");
    this.modal = document.querySelector(".modal");
    this.modalContainer = document.querySelector(".modal__container");
    this.saerchInput = document.querySelector("input");
    this.renderCards();
    // this.bindListener();
  }

  //    CARD
  _createCards(data) {
    const card = document.createElement("div");
    card.className = "card";
    card.onclick = (event) => {
      this.renderModal(data);
    };

    card.innerHTML = `
            <div class="card-body">
              <div class="card-img">
                <img src="images/${data.imgUrl}" alt="product"> 
              </div>
              <div class="card-info">
                <h3 class="aviable">${data.name}</h3> 
                <p class="infText">${data.orderInfo.inStock} left in stock</p> 
                <p class="infText">Price: ${data.price} $</p> 
              </div>
              <div class="card-button"></div>
            </div>
      `;

    const addToCart = document.createElement("button");
    addToCart.className = "card-btn";
    addToCart.innerHTML = "Add to Cart";
    addToCart.addEventListener("click", (event) => {
      event.stopPropagation();
    });
    addToCart.onclick = () => console.log(data, "addToCart");
    card.querySelector(".card-button").appendChild(addToCart);

    const cardFooter = document.createElement("div");
    cardFooter.className = "card-footer";
    card.appendChild(cardFooter);

    if (`${data.orderInfo.inStock}` == 0) {
      addToCart.style.backgroundColor = "grey";
      addToCart.disabled = "true";
    }

    cardFooter.innerHTML = `
    <div class="card__rate">
        <div class="card__rate-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="24px" height="24px">
              <path d="M0 0h24v24H0z" fill="none" />
              <path
                d="M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z"
                fill="#F05454" />
            </svg>
        </div>
        <div class="card__rate-text">
            <p>${data.orderInfo.reviews} positive reviews</p>
        </div>
        <div class="card__rate-value">
            <p>...</p>
        </div>
    </div>
    <div class="card__avarage">
        <p>Above average</p>
        <p>orders</p>
    </div>
    `;

    return card;
  }

  renderCards(arr = items) {
    this.container.innerHTML = "";
    const cards = arr.map((card) => this._createCards(card));
    this.container.append(...cards);
  }

  //    MODAL
  renderModal(data) {
    this.modal.classList.add("active");
    this.modalContainer.innerHTML = `
    <div class="modal__inner">
        <div class="modal__img">
            <div class="modal__img-holder">
                <img src="images/${data.imgUrl}" alt="${data.name}">
            </div>
        </div>
        <div class="modal__info">
            <h3>${data.name}</h3>
            <div class="modal__reviews">
                <div class="card__rate">
                    <div class="card__rate-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="24px" height="24px">
                        <path d="M0 0h24v24H0z" fill="none" />
                        <path
                        d="M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z"
                        fill="#F05454" />
                        </svg>
                    </div>
                    <div class="card__rate-text">
                        <p>${data.orderInfo.reviews} positive reviews</p>
                    </div>
                    <div class="card__rate-value">
                        <p>${Math.round(Math.random() * 1000)}</p>
                    </div>
                </div>
                <div class="card__avarage">
                    <p>Above average</p>
                    <p>orders</p>
                </div>
            </div>
            <div class="modal__features">
                <div><span>Color:</span><span> ${data.color}</span></div>
                <div><span>Operation system:</span><span> ${
                  data.os
                }</span></div>
                <div><span>Chip:</span><span> ${data.chip.name}${
      data.chip.cores
    }</span></div>
                <div><span>Height:</span><span> ${
                  data.size.height
                }cm</span></div>
                <div><span>Width:</span><span> ${data.size.width}cm</span></div>
                <div><span>Depth:</span><span> ${data.size.depth}cm</span></div>
                <div><span>Weight:</span><span> ${
                  data.size.weight
                }g</span></div>
            </div>
        </div>
        <div class="modal__price">
            <div class="modal__price-value">${data.price}$</div>
            <span class="modal__in-stock">Stock: ${
              data.orderInfo.inStock
            }pcs</span>
            <div class="modal__btn-box"></div>
        </div>
    </div>
    `;

    const button = document.createElement("button");
    button.innerHTML = "add to card";
    button.className = "modal__btn";
    button.onclick = () => {
      console.log(data);
    };
    this.modalContainer.querySelector(".modal__price").append(button);

    this.modal.onclick = (event) => {
      if (this.modal !== event.target) return;
      this.modal.classList.remove("active");
    };
  }
}

const cards = new Card();
